terraform {
  backend "s3" {
    bucket = "dns-zones-4a589e31-terraform-state"
    key    = "example-com-dns-zone-iac/terraform.tfstate"
    region = "us-east-1"
  }
}
