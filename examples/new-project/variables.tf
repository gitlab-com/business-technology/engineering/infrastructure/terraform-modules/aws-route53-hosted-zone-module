# variables.tf

###############################################################################
# Each of these variables are defined in terraform.tfvars.json.               #
###############################################################################

variable "dns_zone_fqdn" {
  type        = string
  description = "The FQDN of the DNS zone without a trailing period (Example: domain.tld)"
}

variable "root_a_record" {
  type        = map(any)
  description = "A map with the root A record configuration."
}

variable "root_alias_record" {
  type        = map(any)
  description = "The map with the root Alias record configuration."
}

variable "subzones" {
  type        = map(any)
  description = "A map with the subdomain name and a list of name servers that host the subzone configuration."
  default     = {}
}

variable "a_records" {
  type        = map(any)
  description = "A map with record name and IP address value."
  default     = {}
}

variable "alias_records" {
  type        = map(any)
  description = "A map with Alias record name and FQDN value."
  default     = {}
}

variable "cname_records" {
  type        = map(any)
  description = "A map with record name and CNAME value."
  default     = {}
}

variable "root_mx_records" {
  type        = list(any)
  description = "A list with MX values for top-level domain."
  default     = []
}

variable "subdomain_mx_records" {
  type        = map(any)
  description = "A map with MX record name and MX value list for subdomains"
  default     = {}
}

variable "txt_records" {
  type        = map(any)
  description = "A map with TXT record name and TXT value"
  default     = {}
}
